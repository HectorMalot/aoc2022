package main

import (
	"fmt"
	"strings"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	input := strings.Split(aoc.Input(2022, 13), "\n\n")
	pairs := aoc.Map(input, parse)

	// Problem 1
	res := aoc.Map(pairs, ordered)
	answer := 0
	for i, r := range res {
		if r == '<' {
			answer += i + 1
		}
	}
	pp.Println("Part 1:", answer)

	// Problem 2
	div1 := parsenode(strings.NewReader("[[2]]"))
	div2 := parsenode(strings.NewReader("[[6]]"))
	packets := aoc.Flatten(aoc.Map(pairs, func(p pair) []*node { return []*node{p.left, p.right} }))
	indexes := aoc.Inject(packets, []int{1, 2}, func(n *node, acc []int) []int {
		if n.compare(div1) == '<' {
			return []int{acc[0] + 1, acc[1] + 1}
		}
		if n.compare(div2) == '<' {
			return []int{acc[0], acc[1] + 1}
		}
		return acc
	})
	pp.Println("Part 2:", indexes[0]*indexes[1])
}

type pair struct {
	left, right *node
}

type node struct {
	value int
	subs  []*node
	isVal bool
}

func parse(in string) pair {
	l, r, _ := strings.Cut(in, "\n")
	left := parsenode(strings.NewReader(l))
	right := parsenode(strings.NewReader(r))
	// left := parse*node(bufio.NewReader(strings.NewReader(l)))
	// right := parse*node(bufio.NewReader(strings.NewReader(r)))
	return pair{left: left, right: right}
}

func parsenode(r *strings.Reader) *node {
	n := &node{subs: make([]*node, 0, 5)}
	// n := *node{subs: []*node{}}
	for {
		ch, err := r.ReadByte()
		if err != nil {
			break
		}
		switch ch {
		case '[': // new sub*node
			n.subs = append(n.subs, parsenode(r))
		case ']':
			// this *node is done, hand back to parent
			return n
		case ',':
			// continue
		default: // number (add as *node with val)
			// Read until either , or ]
			body := []byte{ch}
			for {
				nxt, _ := r.ReadByte()
				if nxt == ']' || nxt == ',' {
					r.UnreadByte()
					break
				}
				body = append(body, nxt)
			}
			n.subs = append(n.subs, &node{value: aoc.Atoi(string(body)), isVal: true})
		}
	}
	return n
}

func (n1 *node) compare(n2 *node) rune {
	switch {
	case n1.isVal && n2.isVal: // lower integer should come first
		switch {
		case n1.value < n2.value: // ordered correctly
			return '<'
		case n1.value > n2.value: // swapped
			return '>'
		default: // equal
			return '='
		}
	case !n1.isVal && !n2.isVal: // compare values of lists.
		for idx, n := range n1.subs {
			if idx >= len(n2.subs) { // If right runs out of items first, its swapped
				return '>'
			}
			res := n.compare(n2.subs[idx]) // normal compare of 2 *nodes
			if res != '=' {
				return res
			}
		}
		if len(n2.subs) > len(n1.subs) { // If left runs out of items first, its in order
			return '<'
		}
	default: // comparing int with list
		if n1.isVal {
			newnode := &node{}
			newnode.subs = append(newnode.subs, n1)
			return newnode.compare(n2)
		}
		newnode := &node{}
		newnode.subs = append(newnode.subs, n2)
		return n1.compare(newnode)
	}
	return '='
}

func ordered(p pair) rune {
	res := p.left.compare(p.right)
	return res
}

func node2String(n *node) string {
	if n.isVal {
		return fmt.Sprintf("%d", n.value)
	}
	return fmt.Sprintf("[%s]", strings.Join(aoc.Map(n.subs, node2String), ","))
}
