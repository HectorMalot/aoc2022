package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	input := aoc.Characters(aoc.Input(2022, 6))
	// Problem 1
	lengths := aoc.WindowFn(input, 4, 1, func(body []string) int { return aoc.NewSet(body...).Len() })
	idx := markerPos(lengths, 4)
	pp.Println("Puzzel 1:", idx)

	// Problem 2
	lengths = aoc.WindowFn(input, 14, 1, func(body []string) int { return aoc.NewSet(body...).Len() })
	idx = markerPos(lengths, 14)
	pp.Println("Puzzel 2:", idx)
}

func markerPos(lengths []int, l int) int {
	return l + aoc.MinimumIndex(lengths, func(length int) int {
		if length == l {
			return 0
		} else {
			return 1
		}
	})
}
