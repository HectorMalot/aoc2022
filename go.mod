module gitlab.com/hectormalot/aoc2022

go 1.19

require (
	github.com/k0kubun/pp/v3 v3.2.0
	github.com/stretchr/testify v1.8.1
	golang.org/x/exp v0.0.0-20221114191408-850992195362
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
