package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 15))
	pp.Println(lines)

	// Problem 1
}
