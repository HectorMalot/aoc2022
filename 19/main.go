package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2022/aoc"
	"golang.org/x/exp/slices"
)

var best = make([]int, 33)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 19))
	bps := aoc.Map(lines, parse)

	// Problem 1
	solutions := aoc.Map(bps, func(bp blueprint) state { best = make([]int, 33); return solve(bp, state{oreBots: 1, maxtime: 24}) })
	ql := 0
	for i, s := range solutions {
		ql += (i + 1) * (s.geode)
	}
	fmt.Println("Part 1:", ql) // 1703

	// Problem 2
	solutions = aoc.Map(bps[:3], func(bp blueprint) state {
		best = make([]int, 33)
		return solve(bp, state{oreBots: 1, maxtime: 32})
	})
	answer := 1
	for _, s := range solutions {
		answer *= s.geode
	}
	fmt.Println("Part 2:", answer) // T: 5301

}

type blueprint struct {
	id        int
	ore_ore   int
	clay_ore  int
	obs_ore   int
	obs_clay  int
	geode_ore int
	geode_obs int
	maxOre    int
}

type state struct {
	ore, clay, obs, geode                 int
	oreBots, clayBots, obsBots, geodeBots int
	time                                  int
	maxtime                               int
	prev                                  *state
}

func parse(line string) blueprint {
	bp := blueprint{}
	fmt.Sscanf(line, "Blueprint %d: Each ore robot costs %d ore. Each clay robot costs %d ore. Each obsidian robot costs %d ore and %d clay. Each geode robot costs %d ore and %d obsidian.",
		&bp.id, &bp.ore_ore, &bp.clay_ore, &bp.obs_ore, &bp.obs_clay, &bp.geode_ore, &bp.geode_obs)
	bp.maxOre = aoc.Max([]int{bp.ore_ore, bp.clay_ore, bp.obs_ore})
	return bp
}

func (s state) next(bp blueprint, action string, prev *state, steps int) state {
	s.time += steps
	// Start construction of selected robot
	switch action {
	case "ore":
		s.ore -= bp.ore_ore
	case "clay":
		s.ore -= bp.clay_ore
	case "obs":
		s.ore -= bp.obs_ore
		s.clay -= bp.obs_clay
	case "geode":
		s.ore -= bp.geode_ore
		s.obs -= bp.geode_obs
	default:
	}
	// run robots
	s.ore += s.oreBots * steps
	s.clay += s.clayBots * steps
	s.obs += s.obsBots * steps
	s.geode += s.geodeBots * steps
	//	Complete construction of selected robot
	switch action {
	case "ore":
		s.oreBots++
	case "clay":
		s.clayBots++
	case "obs":
		s.obsBots++
	case "geode":
		s.geodeBots++
	default:
	}
	s.prev = prev
	return s
}

func solve(bp blueprint, s state) state {
	if s.time >= s.maxtime {
		best[s.time] = s.geode
		return s
	}

	// early termination, once we're past the first 10 states
	tleft := s.maxtime - s.time
	if s.time > 10 {
		if (tleft*tleft+tleft)/2+s.geode*tleft < best[s.time] {
			return s.next(bp, "", &s, tleft)
		}
	}

	// determining options
	options := []state{}
	if s.obsBots > 1 { // Can I build a geode
		if s.ore >= bp.geode_ore && s.obs >= bp.geode_obs { //  NOW?
			options = append(options, s.next(bp, "geode", &s, 1))
		} else { // can I build a geode in the future?
			stepsObs := (bp.geode_obs-s.obs)/s.obsBots + aoc.Ternary((bp.geode_obs-s.obs)%s.obsBots == 0, 0, 1) + 1
			stepsOre := (bp.geode_ore-s.ore)/s.oreBots + aoc.Ternary((bp.geode_ore-s.ore)%s.oreBots == 0, 0, 1) + 1
			steps := aoc.Max([]int{stepsObs, stepsOre, 1})
			if steps < tleft {
				options = append(options, s.next(bp, "geode", &s, steps))
			}
		}
	}

	if s.clayBots > 1 && s.obsBots < bp.geode_obs { // Build obsidian now or in the future
		if (s.obsBots*tleft)+s.obs <= tleft*bp.geode_obs {
			if s.clay >= bp.obs_clay && s.ore >= bp.obs_ore { // now
				options = append(options, s.next(bp, "obs", &s, 1))
			} else {
				stepsClay := (bp.obs_clay-s.clay)/s.clayBots + aoc.Ternary((bp.obs_clay-s.clay)%s.clayBots == 0, 0, 1) + 1
				stepsOre := (bp.obs_ore-s.ore)/s.oreBots + aoc.Ternary((bp.obs_ore-s.ore)%s.oreBots == 0, 0, 1) + 1
				steps := aoc.Max([]int{stepsClay, stepsOre, 1})
				if steps < tleft {
					options = append(options, s.next(bp, "obs", &s, steps))
				}
			}
		}
	}

	if s.clayBots < bp.obs_clay { // Build claybot now or in the future
		if (s.clayBots*tleft)+s.clay <= tleft*bp.obs_clay {
			if s.ore >= bp.clay_ore { // now
				options = append(options, s.next(bp, "clay", &s, 1))
			} else { // future
				steps := (bp.clay_ore-s.ore)/s.oreBots + aoc.Ternary((bp.clay_ore-s.ore)%s.oreBots == 0, 0, 1) + 1
				steps = aoc.Max([]int{steps, 1})
				if steps < tleft {
					options = append(options, s.next(bp, "clay", &s, steps))
				}
			}
		}
	}

	if s.oreBots < bp.maxOre { // Build orebot now or in the future
		if (s.oreBots*tleft)+s.ore <= tleft*bp.maxOre {
			if s.ore >= bp.ore_ore {
				options = append(options, s.next(bp, "ore", &s, 1))
			} else {
				steps := (bp.ore_ore-s.ore)/s.oreBots + aoc.Ternary((bp.ore_ore-s.ore)%s.oreBots == 0, 0, 1) + 1
				steps = aoc.Max([]int{steps, 1})
				if steps < tleft {
					options = append(options, s.next(bp, "ore", &s, steps))
				}
			}
		}
	}
	if len(options) == 0 {
		return s.next(bp, "", &s, tleft)
	}

	states := aoc.Map(options, func(action state) state { return solve(bp, action) })
	slices.SortFunc(states, func(a, b state) bool { return a.geode < b.geode })
	r := states[len(states)-1]
	if r.geode > best[s.time+1] {
		best[s.time+1] = r.geode
	}
	return r
}
