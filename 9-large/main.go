package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/hectormalot/aoc2022/aoc"
)

var moves = map[string]aoc.Pos{
	"L": {X: -1, Y: 0},
	"R": {X: 1, Y: 0},
	"U": {X: 0, Y: 1},
	"D": {X: 0, Y: -1},
}

const (
	buffer  = 100
	sbuffer = 10000
)

func main() {
	tStart := time.Now()
	// input := aoc.Lines(aoc.ReadInput("../tmp/day9_input_xmedium.txt")) // 25.1 secs
	input := aoc.Lines(aoc.ReadInput("../tmp/day9_input_large.txt")) // 25.1 secs
	// 139740182

	ch := head(input)
	for i := 0; i < 9; i++ {
		ch = segment(ch)
	}
	r := pcounter(ch)
	visits := <-r
	fmt.Printf("Part 2: %d\n", visits)

	fmt.Println("Total time:      ", time.Since(tStart))
}

func parse(line string) (aoc.Pos, int) {
	var dir string
	var steps int
	fmt.Sscanf(line, "%s %d", &dir, &steps)
	return moves[dir], steps
}

func head(input []string) <-chan []aoc.Pos {
	pos := aoc.Pos{} // starting position is 0,0
	out := make(chan []aoc.Pos, buffer)
	go func() {
		defer close(out)
		next := []aoc.Pos{}
		for i, line := range input {
			move, steps := parse(line)
			for j := 0; j < steps; j++ {
				pos = pos.Add(move.X, move.Y) // move head
				next = append(next, pos)
			}
			if i%sbuffer == 0 {
				out <- next
				next = []aoc.Pos{}
			}
		}
		out <- next // final flush
	}()
	return out
}

func segment(in <-chan []aoc.Pos) <-chan []aoc.Pos {
	pos := aoc.Pos{} // starting position is 0,0
	out := make(chan []aoc.Pos, buffer)
	go func() {
		var ok bool
		defer close(out)
		out <- []aoc.Pos{pos} // initial position
		for v := range in {
			next := []aoc.Pos{}
			for _, p := range v {
				if pos, ok = follow(pos, p); ok {
					next = append(next, pos)

				}
			}
			out <- next
		}
	}()
	return out
}

func follow(self, head aoc.Pos) (aoc.Pos, bool) {
	delta := self.Rel(head)
	if aoc.Abs(delta.X) == 2 {
		self.X += (delta.X) / -2
		if delta.Y < 0 {
			self.Y += 1
		} else if delta.Y > 0 {
			self.Y -= 1
		}
		return self, true
	} else if aoc.Abs(delta.Y) == 2 {
		self.Y += (delta.Y) / -2
		self.X += delta.X * -1
		return self, true
	}
	return self, false
}

// Bonus

// pcounter uses multiple maps under the hood to do parallel writes
// it sorts incoming lists of aoc.Pos values into stacks, which are then sent
// to 20 workers, each managing their own map. (based on pos.X % 20)
func pcounter(in <-chan []aoc.Pos) <-chan int {
	out := make(chan int)

	numBuckets := 20
	var wg, wg2 sync.WaitGroup
	wg.Add(numBuckets)
	feedChans := make([]chan []aoc.Pos, numBuckets)
	resultChan := make(chan map[aoc.Pos]struct{})
	go func() {
		wg.Wait()
		close(resultChan)
	}()
	for i := 0; i < numBuckets; i++ {
		feedChans[i] = make(chan []aoc.Pos)
		go mapWorker(feedChans[i], resultChan)
	}
	go func() {
		defer close(out)
		for v := range in {
			stacks := make([][]aoc.Pos, numBuckets)
			for idx := range stacks {
				stacks[idx] = make([]aoc.Pos, 0, sbuffer)
			}
			for _, p := range v {
				stacks[aoc.Abs(p.X)%numBuckets] = append(stacks[aoc.Abs(p.X)%numBuckets], p)
			}
			wg2.Add(1)
			go func(stacks [][]aoc.Pos) {
				for idx, stack := range stacks {
					feedChans[idx] <- stack
				}
				wg2.Done()
			}(stacks)
		}
		for _, ch := range feedChans {
			wg2.Wait()
			close(ch)
		}
		visits := 0
		for v := range resultChan {
			visits += len(v)
			wg.Done()
		}
		out <- visits
		// fmt.Printf("Counter %s: %d\n", name, visits)
	}()
	return out
}

func mapWorker(in <-chan []aoc.Pos, out chan<- map[aoc.Pos]struct{}) {
	m := make(map[aoc.Pos]struct{}, 100_000)
	for v := range in {
		for _, p := range v {
			m[p] = struct{}{}
		}
	}
	out <- m
}
