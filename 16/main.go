package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
	"golang.org/x/exp/slices"
)

type valve struct {
	id      string
	rate    int
	tunnels []string
}

type cave map[string]valve

type walk struct {
	score int
	route []string
}

// memoisation
var distances = make(map[string]int)

func main() {
	tStart := time.Now()
	lines := aoc.Lines(aoc.Input(2022, 16))
	cave := aoc.Inject(lines, newCave(), func(l string, c cave) cave {
		v := parse(l)
		c[v.id] = v
		return c
	})
	workingValves := aoc.Filter(aoc.Vals(cave), func(v valve) bool { return v.rate > 0 })
	preCalcDistances(workingValves, cave)

	// Problem 1
	a1 := solve1(workingValves, cave, "AA", 30)
	pp.Println("part 1:", a1.score) // T: 1651, P: 2087
	fmt.Println("Time: ", time.Since(tStart))

	// Problem 2: what if we just try opening the remaining valves after the elephant is done?
	a2_1 := solve1(workingValves, cave, "AA", 26)
	remainingValves := aoc.Filter(workingValves, func(v2 valve) bool { return !aoc.Contains(a2_1.route, v2.id) })
	a2_2 := solve1(remainingValves, cave, "AA", 26)
	pp.Println("part 2:", a2_1.score+a2_2.score)
	fmt.Println("Time: ", time.Since(tStart))

}

func preCalcDistances(valves []valve, c cave) {
	for i, a := range valves {
		for _, b := range valves[i:] {
			if a.id == b.id {
				distances[a.id+a.id] = 0
				continue
			}
			dist := c.distance(a.id, b.id)
			distances[a.id+b.id] = dist
			distances[b.id+a.id] = dist
		}
	}
}

func solve1(valves []valve, c cave, curr string, timeLeft int) walk {
	if timeLeft < 1 {
		return walk{}
	}
	scores := aoc.Map(valves, func(v valve) walk {
		// Get score for future walking to each working valve
		if d := c.distance(curr, v.id); d < timeLeft { // we can reach this valve in time
			remainingValves := aoc.Filter(valves, func(v2 valve) bool { return v2.id != v.id })
			sol := solve1(remainingValves, c, v.id, timeLeft-d-1)
			sol.score += (timeLeft - d - 1) * v.rate /* NPV valve */
			// sol.route = append(sol.route, fmt.Sprintf("%s min: %d, value: %d", v.id, timeLeft-d-1, (timeLeft-d-1)*v.rate))
			sol.route = append(sol.route, v.id)
			return sol
		}
		return walk{} // can't reach this valve
	})
	max := aoc.Inject(scores, walk{}, func(w, acc walk) walk {
		return aoc.Ternary(w.score > acc.score, w, acc)
	})
	return max
}

func parse(line string) valve {
	r := regexp.MustCompile(`Valve (\w{2}) has flow rate=(\d+); tunnels? leads? to valves? (.*)`)
	v := valve{}
	matches := r.FindStringSubmatch(line)
	v.id = matches[1]
	v.rate = aoc.Atoi(matches[2])
	v.tunnels = strings.Split(matches[3], ", ")
	return v
}

func newCave() cave {
	return make(map[string]valve)
}

func (c cave) distance(a, b string) int {
	if d, ok := distances[a+b]; ok {
		return d
	}
	d := c.findRoute(a, b, []string{})
	return d
}

func (c cave) findRoute(a, b string, visited []string) int {
	options := c[a].tunnels
	options = aoc.Filter(options, func(option string) bool {
		return !aoc.Contains(visited, option)
	})
	if len(options) == 0 {
		return 1_000_000 // end of path
	}
	if aoc.Contains(options, b) {
		return 1
	}
	routes := aoc.Map(options, func(t string) int {
		v := slices.Clone(visited)
		return 1 + c.findRoute(t, b, append(v, a))
	})
	return aoc.Min(routes)
}
