package main

import (
	"fmt"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 15))
	sensors := aoc.Map(lines, parse)

	// Problem 1
	row := 2000000
	covers := make([]rng, len(sensors))
	for _, s := range sensors {
		if r, ok := s.coverage(row); ok {
			covers = append(covers, r)
		}
	}
	covers = union(covers)
	excluded := aoc.Inject(covers, 0, func(c rng, acc int) int { return acc + c.max - c.min + 1 })
	beacons := 1 // hardcoded: there is 1 beacon on this line
	pp.Println("Part 1:", excluded-beacons)

	// Part 2
	min, max := 0, 4_000_000
	ranges, y := findGap(min, max, sensors)
	x := aoc.Min([]int{ranges[0].max, ranges[1].max}) + 1 // won't work if it's the first or last field
	pp.Println("Part 2:", x*max+y)
}

type sensor struct {
	sensor aoc.Pos
	beacon aoc.Pos
}

type rng struct {
	min, max int
}

func parse(line string) (s sensor) {
	fmt.Sscanf(line, "Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d", &s.sensor.X, &s.sensor.Y, &s.beacon.X, &s.beacon.Y)
	return s
}

func (s sensor) coverage(y int) (c rng, covers bool) {
	dy := aoc.Abs(s.sensor.Y - y)
	db := s.sensor.ManhattanDist(s.beacon)
	if dy > db {
		return rng{}, false
	}
	return rng{min: s.sensor.X - (db - dy), max: s.sensor.X + (db - dy)}, true
}

func union(ranges []rng) []rng {
	if i1, i2, ok := anyOverlap(ranges); ok {
		newRng := reduceRng(ranges[i1], ranges[i2])
		ranges = append(ranges[:i1], ranges[i1+1:]...)
		ranges = append(ranges[:i2-1], ranges[i2:]...)
		ranges = append(ranges, newRng)
	} else {
		return ranges
	}
	return union(ranges)
}

func reduceRng(r1, r2 rng) rng {
	return rng{aoc.Min([]int{r1.min, r2.min}), aoc.Max([]int{r1.max, r2.max})}
}

func anyOverlap(ranges []rng) (int, int, bool) {
	if len(ranges) == 1 {
		return 0, 0, false
	}
	for i, r1 := range ranges {
		for j, r2 := range ranges[(i + 1):] {
			if overlap(r1, r2) {
				return i, i + j + 1, true
			}
		}
	}
	return 0, 0, false
}

func overlap(r1, r2 rng) bool {
	if r1.max < r2.min-1 || r2.max < r1.min-1 {
		return false
	}
	return true
}

func findGap(min, max int, sensors []sensor) ([]rng, int) {
	for y := min; y < max; y++ {
		covers := make([]rng, len(sensors))
		for _, s := range sensors {
			if r, ok := s.coverage(y); ok {
				covers = append(covers, r)
			}
		}
		covers = union(covers)
		if len(covers) != 1 {
			fmt.Printf("Found line with a gap at y: %d", y)
			return covers, y
		}
	}
	return nil, 0
}
