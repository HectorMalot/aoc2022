package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMergeRanges(t *testing.T) {
	one := []rng{
		{min: 10, max: 40},
		{min: 25, max: 60},
		{min: 10, max: 30},
		{min: 5, max: 9},
	}
	two := []rng{
		{min: 10, max: 40},
		{min: 10, max: 30},
		{min: 500, max: 550},
		{min: 25, max: 60},
		{min: 5, max: 9},
		{min: 520, max: 960},
	}
	require.Len(t, union(one), 1)
	require.Len(t, union(two), 2)
}
