package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	tStart := time.Now()
	input := aoc.Map(aoc.Lines(aoc.Input(2022, 20)), aoc.Atoi)
	fmt.Println("Part 1:", part1(input)) // 3466
	fmt.Println("Part 2:", part2(input)) // 9995532008348
	fmt.Println("Time: ", time.Since(tStart))

}

func part1(input []int) int {
	ll, indexes := setup(input)
	ll.Current = run(ll, indexes, 1)
	return answer(ll)
}

func part2(input []int) int {
	input = aoc.Map(input, func(i int) int { return i * 811589153 })
	ll, indexes := setup(input)
	ll.Current = run(ll, indexes, 10)
	return answer(ll)
}

func setup(input []int) (*aoc.LinkedList[int], []*aoc.Node[int]) {
	ll := aoc.NewLinkedList(input)
	ll.Reset()
	indexes := make([]*aoc.Node[int], ll.Len())
	for i := 0; i < ll.Len(); i++ {
		indexes[i] = ll.Current
		ll.Step(1)
	}
	// Make it circular
	ll.Head.Prev = ll.Tail
	ll.Tail.Next = ll.Head
	return ll, indexes
}

func run(ll *aoc.LinkedList[int], indexes []*aoc.Node[int], count int) (zero *aoc.Node[int]) {
	for i := 0; i < count; i++ {
		for _, n := range indexes { // Mixer
			switch {
			case n.Value > 0:
				ll.Current = n
				ll.ExtractNode()
				ll.Step(n.Value % ll.Len())
				ll.InsertNodeBefore(n)
			case n.Value < 0:
				ll.Current = n
				ll.ExtractNode()
				ll.StepBack(aoc.Abs(n.Value) % ll.Len())
				ll.InsertNodeBefore(n)
			default:
				zero = n
			}
		}
	}
	return zero
}

func answer(ll *aoc.LinkedList[int]) (sum int) {
	for i := 0; i < 3; i++ {
		ll.Step(1000)
		sum += ll.Current.Value
	}
	return sum
}
