package main

import (
	"fmt"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 10))

	// Problem 1
	instructions := aoc.Map(lines, parse)
	vals := positions(instructions)
	search := []int{20, 60, 100, 140, 180, 220}
	signal := 0
	for _, s := range search {
		signal += vals[s-1] * s
	}
	pp.Println("Puzzle 1:", signal)

	// Problem 2
	b := aoc.NewLightBoard(40, 6)
	for i, v := range vals {
		if aoc.Abs(i%40-v) <= 1 {
			b.Set(i%40, i/40, true)
		}
	}
	b.Print()
}

func parse(line string) inst {
	switch line[:4] {
	case "noop":
		return inst{time: 1, val: 0}
	case "addx":
		i := inst{time: 2, val: 0}
		fmt.Sscanf(line, "addx %d", &i.val)
		return i
	}
	panic("failed to parse")
}

type inst struct {
	time int
	val  int
}

func positions(ins []inst) (out []int) {
	out = append(out, 1) // starting value
	for _, i := range ins {
		cur := out[len(out)-1]
		for n := 0; n < i.time-1; n++ {
			out = append(out, cur)
		}
		out = append(out, cur+i.val)
	}
	return out
}
