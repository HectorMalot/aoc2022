package main

import (
	"strings"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

var min, max aoc.Pos // let's not recalculate every time

func main() {
	lines := aoc.Lines(aoc.Input(2022, 14))
	rocks := aoc.Map(lines, parse)

	// Problem 1
	cave := parseMap(rocks)
	min, max = aoc.BoundingBox(aoc.Keys(cave))
	input, ok := aoc.Pos{X: 500, Y: 0}, true
	cave[input] = 3
	i := 0
	for { // Insert sand one grain at a time
		sand := input
		sand, ok = flowGrain(sand, cave) // keep moving the grain until it gets stuck.
		if !ok {
			break // Finish when it goes into the void
		}
		cave[sand] = 2
		i++
	}
	pp.Println("part 1: ", i)

	// Problem 2
	rocks = append(rocks, []aoc.Pos{{X: min.X - max.Y, Y: max.Y + 2}, {X: max.X + max.Y, Y: max.Y + 2}}) // add infinite floor
	cave = parseMap(rocks)
	min, max = aoc.BoundingBox(aoc.Keys(cave))
	cave[input] = 3
	i = 0
	for { // Insert sand one grain at a time
		sand := input
		sand, _ = flowGrain(sand, cave) // keep moving the grain until it gets stuck.
		cave[sand] = 2
		i++
		if sand == input {
			break // Finish when it gets stuck at the entrance
		}
	}
	printCave(cave)
	pp.Println("part 2: ", i)

	// pp.Println(aoc.Sum(sums[len(sums)-3:]))
}

func parse(line string) (rocks []aoc.Pos) {
	coords := strings.Split(line, " -> ")
	for _, c := range coords {
		p := aoc.IntList(c)
		rocks = append(rocks, aoc.Pos{X: p[0], Y: p[1]})
	}
	return rocks
}

func parseMap(rocks [][]aoc.Pos) map[aoc.Pos]int {
	cave := make(map[aoc.Pos]int)
	for _, rock := range rocks {
		current := rock[0]
		for _, pos := range rock[1:] {
			for {
				cave[current] = 1
				current = current.StrideTowards(pos)
				if current == pos {
					cave[current] = 1
					break
				}
			}
		}
	}
	return cave
}

func printCave(cave map[aoc.Pos]int) {
	coords := aoc.Keys(cave)
	min, max := aoc.BoundingBox(coords)
	b := aoc.NewBoard[int](max.X-min.X+1, max.Y-min.Y+1)
	aoc.Inject(coords, b, func(p aoc.Pos, b *aoc.Board[int]) *aoc.Board[int] {
		b.Set(p.X-min.X, p.Y-min.Y, cave[p])
		return b
	})
	b.Printf(func(v int) string {
		switch v {
		case 1:
			return "#"
		case 2:
			return "O"
		case 3:
			return "X"
		default:
			return "."
		}
	})
}

func flowGrain(pos aoc.Pos, cave map[aoc.Pos]int) (aoc.Pos, bool) {
	var ok bool
	pos, ok = findBelow(pos, cave) // Fall down
	if !ok {
		return pos, false // Fell into the void
	}
	// Check if I can fall left diagonally
	pLeft := aoc.Pos{X: pos.X - 1, Y: pos.Y + 1}
	if _, ok := cave[pLeft]; !ok {
		return flowGrain(pLeft, cave)
	}
	pRight := aoc.Pos{X: pos.X + 1, Y: pos.Y + 1}
	if _, ok := cave[pRight]; !ok {
		return flowGrain(pRight, cave)
	}

	return pos, true
}

func findBelow(pos aoc.Pos, cave map[aoc.Pos]int) (aoc.Pos, bool) {
	for {
		pos = pos.Add(0, 1)
		if pos.Y > max.Y { // void
			return pos, false
		}
		if _, ok := cave[pos]; ok {
			return pos.Add(0, -1), true
		}
	}
}
