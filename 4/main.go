package main

import (
	"fmt"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 4))
	pairs := aoc.Map(lines, parse)

	// Problem 1
	answer := len(aoc.Filter(aoc.Map(pairs, contains), aoc.IsTrue))
	pp.Println("Puzzle 1:", answer)

	// Problem 2
	answer = len(aoc.Filter(aoc.Map(pairs, overlaps), aoc.IsTrue))
	pp.Println("Puzzle 2:", answer)

}

type pair struct {
	s1, s2, e1, e2 int
}

func parse(line string) pair {
	p := pair{}
	fmt.Sscanf(line, "%d-%d,%d-%d", &p.s1, &p.e1, &p.s2, &p.e2)
	return p
}

func contains(p pair) bool {
	if p.s1 <= p.s2 && p.e1 >= p.e2 { // 1 contains 2
		return true
	}
	if p.s1 >= p.s2 && p.e1 <= p.e2 { // 2 contains 1
		return true
	}
	return false
}

func overlaps(p pair) bool {
	if p.s1 <= p.e2 && p.e1 >= p.s2 { // 1 contains 2
		return true
	}
	if p.s2 <= p.e1 && p.e2 >= p.s1 { // 1 contains 2
		return true
	}
	return false
}
