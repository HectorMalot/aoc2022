package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func TestCollision(t *testing.T) {
	tower := aoc.Reverse([]uint8{
		0b0000000,
		0b0000111,
		0b0000111,
		0b1111111,
	}) // floor + increased height on the rhs
	s := shapes[1] // plus
	require.False(t, collision(tower, s, aoc.Pos{X: 1, Y: 4}))
	require.False(t, collision(tower, s, aoc.Pos{X: 1, Y: 3}))
	require.False(t, collision(tower, s, aoc.Pos{X: 4, Y: 3}))
	require.False(t, collision(tower, s, aoc.Pos{X: 1, Y: 2}))
	require.False(t, collision(tower, s, aoc.Pos{X: 1, Y: 1}))
	require.False(t, collision(tower, s, aoc.Pos{X: 1, Y: 1}))
	require.True(t, collision(tower, s, aoc.Pos{X: 4, Y: 2}))
	require.True(t, collision(tower, s, aoc.Pos{X: 4, Y: 1}))
	require.True(t, collision(tower, s, aoc.Pos{X: 0, Y: 0}))
	require.False(t, collision(tower, s, aoc.Pos{X: 2, Y: 2}))
}
