package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2022/aoc"
)

var (
	shapes = []shape{
		{mask: []uint8{0b1111}, w: 4, h: 1},
		{mask: []uint8{0b010, 0b111, 0b010}, w: 3, h: 3},
		{mask: []uint8{0b111, 0b001, 0b001}, w: 3, h: 3}, // note, array is bottom to top
		{mask: []uint8{0b1, 0b1, 0b1, 0b1}, w: 1, h: 4},
		{mask: []uint8{0b11, 0b11}, w: 2, h: 2},
	}
)

type shape struct {
	mask []uint8
	w, h int
}

func main() {
	pattern := aoc.Characters(aoc.Input(2022, 17))

	// Problem 1
	fmt.Printf("Part 1: %d rock\n", play(pattern, 2022)) // 3168

	// Problem 2: 1_000_000_000_000 rocks
	// Method:
	// - Initial rocks will have variability, the bottom is not part of the sequence
	// - Likely, a consistently repeating pattern will arise after some amount of rocks
	rocks := 1_000_000_000_000
	start, interval := findPattern(pattern)
	h1 := play(pattern, start)
	h2 := play(pattern, start+interval) - h1
	numSeq := (rocks - start) / interval
	roundedHeight := h1 + numSeq*h2
	delta := rocks - (start + numSeq*interval)
	fmt.Printf("Part 2: %d rock\n", roundedHeight+play(pattern, start+delta)-h1) // 1554117647070
}

func play(pattern []string, steps int) int {
	tower := []uint8{0b1111111} // floor
	j := 0
	for i := 0; i < steps; i++ {
		tower, j = nextTower(i, tower, pattern, j)
	}
	return len(tower) - 1
}

func findPattern(pattern []string) (int, int) {
	for x := 0; x < len(pattern); x++ {
		lastI, lastDelta := -1, -1
		tower := []uint8{0b1111111} // floor
		j := 0
		for i := 0; i < 10_000; i++ {
			if i%len(shapes) == 0 && j == x { // Find a specific combination of shape & instruction
				if lastDelta == i-lastI { // Determine if it is recurring
					return lastI, lastDelta
				}
				lastDelta = i - lastI
				lastI = i
			}
			tower, j = nextTower(i, tower, pattern, j)
		}
	}
	return 0, 0
}

func nextTower(i int, tower []uint8, pattern []string, j int) ([]uint8, int) {
	s := shapes[i%len(shapes)]
	p := aoc.Pos{X: 2, Y: len(tower) + 3}
	for {
		// First, move left or right if possible
		j = j % len(pattern)
		if newP := move(s, p, pattern[j%len(pattern)]); !collision(tower, s, newP) {
			p = newP
		}
		j++

		// Then, drop down if possible
		newP := p.Add(0, -1)
		if collision(tower, s, newP) {
			// Add to tower if collision
			return add(tower, s, p), j
		}
		p = newP
	}
}

// position is the lower left coordinate of the shape
func collision(tower []uint8, s shape, pos aoc.Pos) bool {
	if pos.Y >= len(tower) {
		return false // above tower
	}
	for i, r := range s.mask {
		if (pos.Y + i) >= len(tower) {
			return false // rest of shape is above tower
		}
		m := r << (7 - s.w - pos.X)
		if (m & tower[pos.Y+i]) != 0 {
			return true
		}
	}
	return false
}

// moves within the boundaries of the game, does not check collisions
func move(s shape, pos aoc.Pos, direction string) aoc.Pos {
	x := pos.X + aoc.Ternary(direction == ">", 1, -1)
	if x < 0 || x+s.w > 7 { // out of bounds
		return pos
	}
	return aoc.Pos{X: x, Y: pos.Y}
}

func add(t []uint8, s shape, pos aoc.Pos) []uint8 {
	for i, r := range s.mask {
		t = extendToFit(t, pos.Y+i)
		m := r << (7 - s.w - pos.X)
		t[pos.Y+i] = t[pos.Y+i] | m
	}
	return t
}

func extendToFit(t []uint8, y int) []uint8 {
	if y >= len(t) {
		t = append(t, 0) // add rows as needed
		return extendToFit(t, y)
	}
	return t
}
