package main

import (
	"fmt"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	input := aoc.Lines(aoc.Input(2022, 7))
	dirs := &dir{subdirs: make([]*dir, 0), files: make([]file, 0)}
	dirs = aoc.Inject(input, dirs, parse)
	dirs = dirs.root()

	// Problem 1
	sizes := aoc.Map(dirs.allDirs(), func(d *dir) int { return d.size() })
	sumSmall := aoc.Sum(aoc.Filter(sizes, func(s int) bool { return s <= 100_000 }))
	pp.Println("Puzzle 1:", sumSmall)

	// Problem 2
	minreq := 30_000_000
	total := 70_000_000
	used := aoc.Max(sizes)
	search := minreq - (total - used)
	options := aoc.Filter(sizes, func(s int) bool { return s > search })
	pp.Println("Puzzle 2:", aoc.Sort(options)[0])
}

type dir struct {
	name    string
	parent  *dir
	subdirs []*dir
	files   []file
}

type file struct {
	name string
	size int
}

func parse(line string, loc *dir) *dir {
	if line[0] == '$' { // navigation mode
		switch line {
		case "$ cd ..": // move up
			return loc.parent
		case "$ ls": // list contents
			return loc
		default: // moving into dir
			newdir := &dir{parent: loc}
			fmt.Sscanf(line, "$ cd %s", &newdir.name)
			loc.subdirs = append(loc.subdirs, newdir)
			// pp.Println("creating dir", newdir.path())
			return newdir
		}
	} else { // printing mode
		if line[:3] != "dir" { // ignoring directories, files only
			f := file{}
			fmt.Sscanf(line, "%d %s", &f.size, &f.name)
			loc.files = append(loc.files, f)
		}
	}
	return loc
}

func (d *dir) size() int {
	files := aoc.Sum(aoc.Map(d.files, func(f file) int { return f.size }))
	dirs := aoc.Sum(aoc.Map(d.subdirs, func(d *dir) int { return d.size() }))
	return files + dirs
}

func (d *dir) allDirs() []*dir {
	dirs := make([]*dir, 0)
	dirs = append(dirs, d.subdirs...)
	for _, d := range d.subdirs {
		dirs = append(dirs, d.allDirs()...)
	}
	return dirs
}

func (d *dir) root() *dir {
	if d.parent != nil {
		return d.parent.root()
	}
	return d
}
