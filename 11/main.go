package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	// Problem 1
	monkeys := aoc.Map(strings.Split(aoc.Input(2022, 11), "\n\n"), parse)
	aoc.Map(monkeys, func(m *monkey) int { m.truep = monkeys[m.true]; m.falsep = monkeys[m.false]; return 0 })
	// Run the rounds
	for i := 0; i < 20; i++ {
		for _, m := range monkeys {
			m.Act(1)
		}
	}
	inspections := aoc.Reverse(aoc.Sort(aoc.Map(monkeys, func(m *monkey) int { return m.inspections })))
	pp.Println("Puzzle 1:", inspections[0]*inspections[1])

	// Problem 2
	monkeys = aoc.Map(strings.Split(aoc.Input(2022, 11), "\n\n"), parse)
	aoc.Map(monkeys, func(m *monkey) int { m.truep = monkeys[m.true]; m.falsep = monkeys[m.false]; return 0 })
	mod := aoc.Inject(monkeys, 1, func(m *monkey, mod int) int { return mod * m.test })
	// Run the rounds
	for i := 0; i < 10000; i++ {
		for _, m := range monkeys {
			m.Act(mod)
		}
	}
	inspections = aoc.Reverse(aoc.Sort(aoc.Map(monkeys, func(m *monkey) int { return m.inspections })))
	pp.Println("Puzzle 2:", inspections[0]*inspections[1])
}

type monkey struct {
	id                int
	inspections       int
	items             []int
	operation         func(int) int
	test, true, false int
	truep, falsep     *monkey
}

func parse(in string) *monkey {
	m := &monkey{}
	lines := aoc.Lines(in)
	m.id = aoc.Atoi(lines[0][7:8])
	// Items
	_, l, _ := strings.Cut(lines[1], ": ")
	m.items = aoc.IntList(l)
	// Operation
	switch lines[2][23] {
	case '*':
		if i, err := strconv.Atoi(lines[2][25:]); err == nil {
			m.operation = func(v int) int { return v * i }
		} else {
			m.operation = func(v int) int { return v * v } // squaring
		}
	case '+':
		if i, err := strconv.Atoi(lines[2][25:]); err == nil {
			m.operation = func(v int) int { return (v + i) }
		} else {
			m.operation = func(v int) int { return (v + v) } // doubling
		}
	}
	// Test (is always division)
	m.test = aoc.Atoi(lines[3][21:])
	m.true = aoc.Atoi(lines[4][29:])
	m.false = aoc.Atoi(lines[5][30:])
	return m
}

func (m *monkey) String() string {
	return fmt.Sprintf("Monkey %d (%d): %v", m.id, m.inspections, m.items)
}

func (m *monkey) AddItem(i int) {
	m.items = append(m.items, i)
}

func (m *monkey) Act(mod int) {
	for _, item := range m.items {
		m.inspections++
		if mod == 1 { // Part 1
			item = m.operation(item) / 3
		} else { // Part 2
			item = m.operation(item) % mod
		}
		if item%m.test == 0 {
			m.truep.AddItem(item)
		} else {
			m.falsep.AddItem(item)
		}
	}
	m.items = []int{}
}
