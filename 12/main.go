package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 12))
	heights, finish := parse(lines)
	steps := make(map[aoc.Pos]int)

	// Problem 1
	// Set initial position
	for k, v := range heights {
		if v == 0 {
			steps[k] = 0
		}
	}
	for {
		if !run(heights, steps) {
			break
		}
	}
	fmt.Printf("Part 1, Steps from start to finish: %d\n", steps[finish])

	// Problem 2: any 'a' as starting position
	// Set initial position
	steps = make(map[aoc.Pos]int)
	for k, v := range heights {
		if v == 1 {
			steps[k] = 0
		}
	}

	for {
		if !run(heights, steps) {
			break
		}
	}
	fmt.Printf("Part 2, Steps from start to finish: %d\n", steps[finish])

}

func parse(lines []string) (map[aoc.Pos]int, aoc.Pos) {
	heights := make(map[aoc.Pos]int)
	finish := aoc.Pos{}
	for y, line := range lines {
		for x, height := range line {
			switch height {
			case 'S':
				heights[aoc.Pos{X: x, Y: y}] = 0
			case 'E':
				heights[aoc.Pos{X: x, Y: y}] = 27
				finish = aoc.Pos{X: x, Y: y}
			default:
				heights[aoc.Pos{X: x, Y: y}] = int(height) - 96
			}
		}
	}
	return heights, finish
}

func run(heights, steps map[aoc.Pos]int) bool {
	changed := false
	for k, v := range steps {
		moves := k.Moves()
		curHeight := heights[k]
		for _, move := range moves {
			if h, ok := heights[move]; ok { // move in on the map
				if h <= curHeight+1 { // it's a valid step (max 1 up)
					// update if: steps is empty (i.e. unvisited) OR value in steps is higher, and i can move, this is a faster route)
					if mv, ok := steps[move]; !ok || mv > v+1 {
						steps[move] = v + 1
						changed = true
					}
				}
			}
		}
	}
	return changed
}
