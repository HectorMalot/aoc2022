package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 18))
	coords := aoc.Map(aoc.Map(lines, aoc.IntList), func(in []int) aoc.Pos3D { return aoc.Pos3D{X: in[0], Y: in[1], Z: in[2]} })

	// Problem 1
	s := aoc.NewSet(coords...)
	surface := aoc.Inject(coords, 0, func(p aoc.Pos3D, surface int) int {
		sn := p.Moves()
		return len(aoc.Filter(sn, func(n aoc.Pos3D) bool { return !s.Contains(n) })) + surface
	})
	fmt.Println("Part 1:", surface)

	// Problem 2
	min, max := aoc.BoundingBox3D(coords)
	min = aoc.Pos3D{X: min.X - 1, Y: min.Y - 1, Z: min.Z - 1}
	max = aoc.Pos3D{X: max.X + 1, Y: max.Y + 1, Z: max.Z + 1}
	within := aoc.WithinBoundingBox3D(min, max)
	v := aoc.NewSet(max) // initial position
	fmt.Println("Part 2:", FindSurface(min, s, v, within))
}

func FindSurface(curr aoc.Pos3D, solids, visited *aoc.Set[aoc.Pos3D], within func(aoc.Pos3D) bool) int {
	l := visited.Len()
	surface := 0
	nodes := curr.Moves()
	// filter out anything outside of the bounding box, or anything already visited
	nodes = aoc.Filter(nodes, func(n aoc.Pos3D) bool { return within(n) && !visited.Contains(n) })

	// any hit with solids is a surface
	surface += aoc.Inject(nodes, 0, func(n aoc.Pos3D, acc int) int { return aoc.Ternary(solids.Contains(n), acc+1, acc) })

	// Nodes to visit are those not being a solid
	visited.Add(curr)
	nodes = aoc.Filter(nodes, func(n aoc.Pos3D) bool { return !solids.Contains(n) && !visited.Contains(n) })

	// This is a hack. The set is a pointer and gets updated recursively (not so pure)
	// with l==1 one, we know that this is the top of the recursion tree
	// then we check all the surfaces hitting the outside
	// a direct Map/Inject doens't work
	for _, n := range nodes {
		FindSurface(n, solids, visited, within)
	}
	if l == 1 {
		locs := visited.Elems()
		c2 := aoc.Map(locs, func(n aoc.Pos3D) []aoc.Pos3D { return n.Moves() })
		s2 := aoc.Map(aoc.Flatten(c2), func(c aoc.Pos3D) int {
			if solids.Contains(c) {
				return 1
			}
			return 0
		})
		return aoc.Sum(s2)
	}
	return surface
}
