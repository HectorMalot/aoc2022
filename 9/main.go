package main

import (
	"fmt"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

var moves = map[string]aoc.Pos{
	"L": {X: -1, Y: 0},
	"R": {X: 1, Y: 0},
	"U": {X: 0, Y: 1},
	"D": {X: 0, Y: -1},
}

var follows = map[aoc.Pos]aoc.Pos{
	// to the left to the left
	{X: -2, Y: -2}: {X: +1, Y: +1},
	{X: -2, Y: -1}: {X: +1, Y: +1},
	{X: -2, Y: +0}: {X: +1, Y: +0},
	{X: -2, Y: +1}: {X: +1, Y: -1},
	{X: -2, Y: +2}: {X: +1, Y: -1},
	// to the right to the right
	{X: +2, Y: -2}: {X: -1, Y: +1},
	{X: +2, Y: -1}: {X: -1, Y: +1},
	{X: +2, Y: +0}: {X: -1, Y: +0},
	{X: +2, Y: +1}: {X: -1, Y: -1},
	{X: +2, Y: +2}: {X: -1, Y: -1},
	// Timber!
	{X: -1, Y: +2}: {X: +1, Y: -1},
	{X: +0, Y: +2}: {X: +0, Y: -1},
	{X: +1, Y: +2}: {X: -1, Y: -1},
	// Dropit!
	{X: -1, Y: -2}: {X: +1, Y: +1},
	{X: +0, Y: -2}: {X: +0, Y: +1},
	{X: +1, Y: -2}: {X: -1, Y: +1},
}

func main() {
	input := aoc.Lines(aoc.Input(2022, 9))
	moves := aoc.Flatten(aoc.Map(input, parse))

	// Problem 1: # of visited fields by tail: 6464
	rope := []aoc.Pos{{X: 0, Y: 0}, {X: 0, Y: 0}}
	visits := map[aoc.Pos]int{}
	for _, move := range moves {
		rope = step(rope, move)
		visits[rope[len(rope)-1]]++
	}
	pp.Println("Puzzle 1:", len(visits))

	// Problem 2
	rope = []aoc.Pos{}
	for i := 0; i < 10; i++ {
		rope = append(rope, aoc.Pos{})
	}
	visits = map[aoc.Pos]int{}
	for _, move := range moves {
		rope = step(rope, move)
		visits[rope[len(rope)-1]]++
	}
	pp.Println("Puzzle 2:", len(visits))
}

func parse(line string) (out []aoc.Pos) {
	var dir string
	var steps int
	fmt.Sscanf(line, "%s %d", &dir, &steps)
	for i := 0; i < steps; i++ {
		out = append(out, moves[dir])
	}
	return out
}

func step(rope []aoc.Pos, m aoc.Pos) []aoc.Pos {
	rope[0] = rope[0].Add(m.X, m.Y)
	for i := 1; i < len(rope); i++ {
		delta := rope[i].Rel(rope[i-1])
		rope[i] = rope[i].Add(follows[delta].X, follows[delta].Y)
	}
	return rope
}
