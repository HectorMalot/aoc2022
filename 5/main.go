package main

import (
	"fmt"
	"strings"

	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	parts := strings.Split(aoc.Input(2022, 5), "\n\n")
	containers := parseContainers(aoc.Lines(parts[0]))
	steps := aoc.Map(aoc.Lines(parts[1]), parseStep)

	// Problem 1
	final := aoc.Inject(steps, containers, apply)
	answer1 := make([]byte, len(final))
	for k, v := range final {
		answer1[k-1] = v[len(v)-1]
	}
	pp.Println("Puzzel 1:", string(answer1))

	// Problem 2
	containers = parseContainers(aoc.Lines(parts[0]))
	final = aoc.Inject(steps, containers, apply2)
	answer2 := make([]byte, len(final))
	for k, v := range final {
		answer2[k-1] = v[len(v)-1]
	}
	pp.Println("Puzzle 2:", string(answer2))

}

type step struct {
	num, from, to int
}

type ship map[int][]byte

func parseStep(line string) step {
	s := step{}
	fmt.Sscanf(line, "move %d from %d to %d", &s.num, &s.from, &s.to)
	return s
}

func parseContainers(lines []string) ship {
	fmt.Println(len(lines))
	length := aoc.Max(aoc.Map(lines, func(line string) int { return len(line) })) / 4
	fmt.Println(length / 4)
	s := make(ship)
	for i := 0; i <= length; i++ {
		for _, line := range lines {
			idx := 1 + i*4
			if line[idx] != ' ' {
				s[i+1] = append(s[i+1], line[idx])
			}
		}
		s[i+1] = []byte(aoc.ReverseString(string(s[i+1])))[1:]
	}
	return s
}

func apply(st step, sh ship) ship {
	for i := 0; i < st.num; i++ {
		sh[st.from], sh[st.to] = sh[st.from][:len(sh[st.from])-1], append(sh[st.to], sh[st.from][len(sh[st.from])-1])
	}
	return sh
}

func apply2(st step, sh ship) ship {
	sh[st.from], sh[st.to] = sh[st.from][:len(sh[st.from])-st.num], append(sh[st.to], sh[st.from][len(sh[st.from])-st.num:]...)
	return sh
}
