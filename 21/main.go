package main

import (
	"fmt"
	"strings"

	"github.com/Knetic/govaluate"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	params := aoc.Inject(aoc.Lines(aoc.Input(2022, 21)), make(map[string]string), parse)
	calc := rep("root", params)

	// Problem 1
	exp, _ := govaluate.NewEvaluableExpression(calc)
	res, _ := exp.Evaluate(nil)
	fmt.Println("Part 1: ", int64(res.(float64)))

	// Problem 2
	left, right, _ := strings.Cut(params["root"], " + ")
	search := int64(0)
	for {
		params["humn"] = fmt.Sprint(search)
		exp1, _ := govaluate.NewEvaluableExpression(rep(left, params))
		res1, _ := exp1.Evaluate(nil)
		exp2, _ := govaluate.NewEvaluableExpression(rep(right, params))
		res2, _ := exp2.Evaluate(nil)
		params["humn"] = fmt.Sprint(search + 1)
		exp3, _ := govaluate.NewEvaluableExpression(rep(left, params))
		res3, _ := exp3.Evaluate(nil)
		exp4, _ := govaluate.NewEvaluableExpression(rep(right, params))
		res4, _ := exp4.Evaluate(nil)

		d1 := int64(res1.(float64)) - int64(res2.(float64))
		if d1 == 0 {
			fmt.Println("Part 2: ", search) // 3721298272959
			break
		}
		d2 := int64(res3.(float64)) - int64(res4.(float64))
		delta := d1 - d2
		search += d1 / delta
	}
}

func parse(line string, out map[string]string) map[string]string {
	k, v, ok := strings.Cut(line, ": ")
	if ok {
		out[k] = v
	}
	return out
}

func rep(node string, params map[string]string) string {
	calc := params[node]
	if len(calc) > 4 && calc[4] == ' ' { // need to recurse: (AAAA * BBBB)
		left, op, right := calc[:4], calc[5], calc[7:]
		return fmt.Sprintf("(%s %s %s)", rep(left, params), string(op), rep(right, params))
	}
	return calc
}
