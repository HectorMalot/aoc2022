package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 3))
	digits := aoc.Map(lines, func(in string) (out []int) {
		for _, ch := range in {
			v := int(ch)
			if v > 96 {
				v = v - 96
			} else {
				v = v - 38
			}
			out = append(out, v)
		}
		return out
	})

	// Problem 1
	split := aoc.Map(digits, func(in []int) (out tuple2[[]int, []int]) {
		out.v1 = in[:len(in)/2]
		out.v2 = in[len(in)/2:]
		return out
	})
	scores := aoc.Map(split, func(in tuple2[[]int, []int]) int {
		for _, n1 := range in.v1 {
			if aoc.Contains(in.v2, n1) {
				return n1
			}
		}
		return 0
	})
	pp.Println(aoc.Sum(scores))

	// Problem 2
	scores2 := aoc.WindowFn(digits, 3, 3, func(in [][]int) int {
		for i := 1; i <= 52; i++ {
			if aoc.Contains(in[0], i) && aoc.Contains(in[1], i) && aoc.Contains(in[2], i) {
				return i
			}
		}
		return 0
	})
	pp.Println(aoc.Sum(scores2))
	// aoc.SubmitAnswer(2022, 3, 2, aoc.Sum(scores2))

}

type tuple2[T1, T2 any] struct {
	v1 T1
	v2 T2
}
