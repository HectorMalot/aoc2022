package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	input := aoc.Map(aoc.Lines(aoc.Input(2022, 8)), aoc.Digits)
	forest := aoc.NewBoard[int](len(input[0]), len(input))
	for y, row := range input {
		forest.SetRow(y, row)
	}
	// pp.Println(forest)

	// Problem 1 : visibility from edges
	visibility := aoc.NewBoard[bool](len(input[0]), len(input))
	for y, row := range *forest {
		for x, v := range row {
			col := forest.Col(x)
			visibility.Set(x, y, visible(x, y, v, row, col))
		}
	}
	pp.Println("Puzzle 1:", len(aoc.Filter(visibility.Elements(), aoc.IsTrue)))

	// Problem 2
	s := aoc.NewBoard[int](len(input[0]), len(input))
	for y, row := range *forest {
		for x, v := range row {
			col := forest.Col(x)
			s.Set(x, y, scenic(x, y, v, row, col))
		}
	}
	pp.Println("Puzzle 2:", aoc.Max(s.Elements()))
}

func visible(x, y, v int, row, col []int) bool {
	left, right, above, below := aoc.Max(row[:x]), aoc.Max(row[x+1:]), aoc.Max(col[:y]), aoc.Max(col[y+1:])
	return aoc.Or(left < v, right < v, above < v, below < v, x == 0, y == 0, x == len(row)-1, y == len(row)-1)
}

func scenic(x, y, v int, row, col []int) int {
	left, right, above, below := aoc.Reverse(row[:x]), row[x+1:], aoc.Reverse(col[:y]), col[y+1:]
	s := FindFirst(left, func(t int) bool { return t >= v }) + 1
	s *= FindFirst(right, func(t int) bool { return t >= v }) + 1
	s *= FindFirst(above, func(t int) bool { return t >= v }) + 1
	s *= FindFirst(below, func(t int) bool { return t >= v }) + 1
	return s
}

// FindFirst returns the index of the element where fn(Element) is true
// Otherwise it returns the last index
func FindFirst[E any](elems []E, fn func(E) bool) int {
	if len(elems) == 0 {
		return 0
	}
	for i, v := range elems {
		if fn(v) {
			return i
		}
	}
	return len(elems) - 1
}
