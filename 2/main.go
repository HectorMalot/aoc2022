package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

type play struct {
	In  int // 1, 2, 3 for rock, paper, siccors
	Out int
}

func main() {
	plays := aoc.Map(aoc.Lines(aoc.Input(2022, 2)), func(line string) (p play) {
		p.In, p.Out = int(line[0]-uint8('@')), int(line[2]-uint8('W'))
		return p
	})

	// Problem 1
	pp.Println(aoc.Sum(aoc.Map(plays, score1)))

	// Problem 2
	pp.Println(aoc.Sum(aoc.Map(plays, score2)))
}

func score1(p play) int {
	switch {
	case p.In == p.Out:
		return p.Out + 3 // Draw
	case p.In-p.Out == 1 || p.In-p.Out == -2:
		return p.Out + 0 // Loss
	case p.In-p.Out == -1 || p.In-p.Out == 2:
		return p.Out + 6 // Win
	}
	return 0
}

func score2(p play) int {
	win := map[int]int{
		1: 2,
		2: 3,
		3: 1,
	}
	lose := map[int]int{
		1: 3,
		2: 1,
		3: 2,
	}
	switch p.Out {
	case 1: // Loss
		return 0 + lose[p.In]
	case 2: // Draw
		return 3 + p.In
	default: // Win
		return 6 + win[p.In]
	}
}
