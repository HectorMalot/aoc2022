package main

import (
	"github.com/k0kubun/pp/v3"
	"gitlab.com/hectormalot/aoc2022/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2022, 1))
	// pp.Println(lines)

	// Problem 1
	pp.Println(aoc.Max(solve(lines)))

	// Problem 2
	sums := solve(lines)
	sums = aoc.Sort(sums)
	pp.Println(aoc.Sum(sums[len(sums)-3:]))
}

func solve(lines []string) (sums []int) {
	current := 0
	for _, v := range lines {
		if v == "" {
			sums = append(sums, current)
			current = 0
			continue
		}
		current += aoc.Atoi(v)
	}
	return sums
}
