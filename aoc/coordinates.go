package aoc

import (
	"fmt"
	"math"
)

type Pos struct {
	X, Y int
}

func (p Pos) ManhattanDist(q Pos) int {
	return Abs(p.X-q.X) + Abs(p.Y-q.Y)
}

func (p Pos) Moves() []Pos {
	return []Pos{
		{p.X, p.Y - 1},
		{p.X, p.Y + 1},
		{p.X - 1, p.Y},
		{p.X + 1, p.Y},
	}
}

// Numpad returns the 8 surrounding fields, including diagonals
func (p Pos) Numpad() []Pos {
	return []Pos{
		{p.X - 1, p.Y - 1},
		{p.X - 1, p.Y + 0},
		{p.X - 1, p.Y + 1},
		{p.X + 0, p.Y - 1},
		{p.X + 0, p.Y + 1},
		{p.X + 1, p.Y - 1},
		{p.X + 1, p.Y + 0},
		{p.X + 1, p.Y + 1},
	}
}

// StrideTowards "moves" p towards q, taking the smallest "step" that results in
// integer coordinates.
func (p Pos) StrideTowards(q Pos) Pos {
	if q == p {
		return q
	}
	dx, dy := q.X-p.X, q.Y-p.Y
	gcd := GCD(dx, dy)
	return Pos{p.X + dx/gcd, p.Y + dy/gcd}
}

func (p Pos) Add(x, y int) Pos {
	return Pos{p.X + x, p.Y + y}
}

func (p Pos) Rel(q Pos) Pos {
	return Pos{p.X - q.X, p.Y - q.Y}
}

func (p Pos) Equal(q Pos) bool {
	return p.X == q.X && p.Y == q.Y
}

func (p Pos) Polar() (rho float64, phi float64) {
	x, y := float64(p.X), float64(p.Y)
	return math.Hypot(x, y), math.Atan2(y, x)
}

func (p Pos) RotateClockwiseAround(q Pos, deg float64) Pos {
	rho, phi := p.Rel(q).Polar()
	phi -= (deg / 180) * math.Pi
	return FromPolar(rho, phi).Add(q.X, q.Y)
}

func (p Pos) RotateCounterClockwiseAround(q Pos, deg float64) Pos {
	rho, phi := p.Rel(q).Polar()
	phi += (deg / 180) * math.Pi
	return FromPolar(rho, phi).Add(q.X, q.Y)
}

func FromPolar(rho, phi float64) Pos {
	return Pos{int(math.Round(rho * math.Cos(phi))), int(math.Round(rho * math.Sin(phi)))}
}

func BoundingBox(ps []Pos) (min, max Pos) {
	min.X, min.Y = int(1e9), int(1e9)
	max.X, max.Y = int(-1e9), int(-1e9)
	for _, p := range ps {
		min.X = Min([]int{min.X, p.X})
		min.Y = Min([]int{min.Y, p.Y})
		max.X = Max([]int{max.X, p.X})
		max.Y = Max([]int{max.Y, p.Y})
	}
	return
}

func PrintPositions(ps []Pos, empty, full rune) {
	min, max := BoundingBox(ps)
	for i := range ps {
		ps[i].X -= min.X
		ps[i].Y -= min.Y
	}
	g := make([][]rune, 1+max.Y-min.Y)
	for y := range g {
		g[y] = make([]rune, 1+max.X-min.X)
		for x := range g[y] {
			g[y][x] = empty
		}
	}
	for _, p := range ps {
		y := len(g) - p.Y - 1 // flip vertically
		g[y][p.X] = full
	}
	for _, row := range g {
		fmt.Println(string(row))
	}
}

type Grid struct {
	X, Y int
}

func (g Grid) Valid(p Pos) bool {
	return 0 <= p.X && p.X <= g.X && 0 <= p.Y && p.Y <= g.Y
}

// Locate returns the position of byte in a grid
func Locate[T comparable](grid [][]T, c T) Pos {
	for y := range grid {
		for x, v := range grid[y] {
			if v == c {
				return Pos{x, y}
			}
		}
	}
	panic("not found")
}

type LightBoard struct {
	board [][]bool
}

func (l *LightBoard) Set(x, y int, lit bool) {
	l.board[y][x] = lit
}

func (l *LightBoard) Get(x, y int) bool {
	return l.board[y][x]
}

func (l *LightBoard) Toggle(x, y int) {
	l.board[y][x] = !l.board[y][x]
}

func (l *LightBoard) Print() {
	for y := range l.board {
		for x := range l.board[y] {
			if l.board[y][x] {
				fmt.Print("#")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

// NewLightBoard setups a grid of on/off switched (2D)
func NewLightBoard(x, y int) *LightBoard {
	board := make([][]bool, y+1)
	for i := range board {
		board[i] = make([]bool, x+1)
	}
	return &LightBoard{board}
}

// Board is a 2D fixed size board of any element
// coordinates are from left top (0,0 is left top)
type Board[T any] [][]T

func NewBoard[T any](x, y int) *Board[T] {
	board := make(Board[T], y)
	for i := range board {
		board[i] = make([]T, x)
	}
	return &board
}

func (b Board[T]) Print() {
	for _, row := range b {
		for _, v := range row {
			fmt.Print(v)
		}
		fmt.Print("\n")
	}
	fmt.Print("\n")
}

func (b Board[T]) Printf(fn func(v T) string) {
	for _, row := range b {
		for _, v := range row {
			fmt.Print(fn(v))
		}
		fmt.Print("\n")
	}
	fmt.Print("\n")
}

func (b Board[T]) Row(y int) []T {
	return b[y]
}

func (b Board[T]) Col(x int) []T {
	var col []T
	for _, row := range b {
		col = append(col, row[x])
	}
	return col
}

func (b Board[T]) SetRow(y int, values []T) {
	b[y] = values
}

func (b Board[T]) Size() (x, y int) {
	if len(b) == 0 {
		return 0, 0
	}
	return len(b[0]), len(b)
}

func (b Board[T]) Get(x, y int) T {
	return b[y][x]
}

func (b Board[T]) Set(x, y int, v T) {
	b[y][x] = v
}

func (b Board[T]) Apply(fn func(T)) {
	for _, row := range b {
		for _, e := range row {
			fn(e)
		}
	}
}

func (b Board[T]) AllValid(fn func(e T) bool) bool {
	for _, row := range b {
		for _, e := range row {
			if !fn(e) {
				return false
			}
		}
	}
	return true
}

func (b Board[T]) AllValidRow(row int, fn func(e T) bool) bool {
	for _, e := range b.Row(row) {
		if !fn(e) {
			return false
		}
	}
	return true
}

func (b Board[T]) AllValidCol(col int, fn func(e T) bool) bool {
	for _, e := range b.Col(col) {
		if !fn(e) {
			return false
		}
	}
	return true
}

func (b Board[T]) Elements() (res []T) {
	for _, row := range b {
		res = append(res, row...)
	}
	return res
}

func (b *Board[T]) Inject(acc int, fn func(int, T) int) int {
	for _, row := range *b {
		for _, val := range row {
			acc = fn(acc, val)
		}
	}
	return acc
}
