package aoc

import (
	"fmt"
	"time"
)

type Timer struct {
	times []timing
}

type timing struct {
	name  string
	value time.Time
}

func NewTimer(name string) *Timer {
	return &Timer{times: []timing{{name: name, value: time.Now()}}}
}

func (t *Timer) Add(name string) {
	t.times = append(t.times, timing{name: name, value: time.Now()})
}

func (t *Timer) String() (out string) {
	max := Inject(t.times, 0, func(t timing, acc int) int { return Max([]int{acc, len(t.name)}) })
	formatString := fmt.Sprintf("%%s%%%ds: %%s\n", max)
	last := t.times[0].value
	for _, tm := range t.times {
		out = fmt.Sprintf(formatString, out, tm.name, tm.value.Sub(last).Round(time.Millisecond))
		last = tm.value
	}
	out = fmt.Sprintf(formatString, out, "Total", last.Sub(t.times[0].value).Round(time.Millisecond))
	return out
}
