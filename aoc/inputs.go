package aoc

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

// Input returns the input for the specified year and day as a string,
// downloading it if it does not already exist on disk.
func Input(year, day int) string {
	filename := fmt.Sprintf("day%v_input.txt", day)
	if _, err := os.Stat(filename); err != nil {
		est, err := time.LoadLocation("EST")
		if err != nil {
			panic(err)
		}
		if t := time.Date(year, time.December, day, 0, 0, 0, 0, est); time.Until(t) > 0 {
			fmt.Printf("Puzzle not unlocked yet! Sleeping for %v...\n", time.Until(t))
			time.Sleep(time.Until(t) + 3*time.Second) // don't want to fire too early
		}
		fmt.Println("Downloading input...")
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("https://adventofcode.com/%v/day/%v/input", year, day), nil)
		req.AddCookie(&http.Cookie{
			Name:  "session",
			Value: os.Getenv("AOC_USER_ID"),
		})
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		if err := os.WriteFile(filename, data, 0660); err != nil {
			panic(err)
		}
	}
	return ReadInput(filename)
}

// Loads day$_input_test.txt instead of day$_input.txt
func TestInput(year, day int) string {
	filename := fmt.Sprintf("day%v_input_test.txt", day)
	return ReadInput(filename)
}

// ReadInput returns the contents of filename as a string.
func ReadInput(filename string) string {
	data, err := os.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	return string(bytes.TrimRight(data, " \t\n\v\f\r"))
}

func SubmitAnswer(year, day, level, answer int) string {
	body := url.Values{
		"level":  {Itoa(level)},
		"answer": {Itoa(answer)},
		// "submit": {"[submit]"},
	}
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("https://adventofcode.com/%v/day/%v/answer", year, day), strings.NewReader(body.Encode()))
	req.AddCookie(&http.Cookie{
		Name:  "session",
		Value: os.Getenv("AOC_USER_ID"),
	})
	req.Header["User-Agent"] = []string{"golang aoc lib by Dennis"}
	req.Header["Content-Type"] = []string{"application/x-www-form-urlencoded"}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	result := string(data)
	return getResult(result)
}

func getResult(body string) string {
	split := strings.Split(body, "<article><p>")
	split = strings.Split(split[1], "</p></article>")
	return split[0]
}
