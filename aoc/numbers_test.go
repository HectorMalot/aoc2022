package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNumbers(t *testing.T) {
	t.Run("max", func(t *testing.T) {
		require.Equal(t, 6, Max([]int{1, 2, 3, 4, 5, 6, 2, 3, 4}))
	})

	t.Run("GCD", func(t *testing.T) {
		require.Equal(t, 4, GCD(8, 12))
	})

	t.Run("LCM", func(t *testing.T) {
		require.Equal(t, 24, LCM(3, 4, 8))
	})
}

func TestWindow(t *testing.T) {
	input := []int{1, 2, 3, 4, 5, 6}
	windowed := WindowFn(input, 3, 1, func(t []int) int { return Sum(t) })
	require.Equal(t, []int{6, 9, 12, 15}, windowed)

	windowed = WindowFn(input, 2, 2, func(t []int) int { return Sum(t) })
	require.Equal(t, []int{3, 7, 11}, windowed)
}
