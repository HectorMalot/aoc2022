package aoc

type Tuple2[T1, T2 any] struct {
	v1 T1
	v2 T2
}

type Tuple3[T1, T2, T3 any] struct {
	v1 T1
	v2 T2
	v3 T3
}

type Tuple4[T1, T2, T3, T4 any] struct {
	v1 T1
	v2 T2
	v3 T3
	v4 T4
}

type Tuple5[T1, T2, T3, T4, T5 any] struct {
	v1 T1
	v2 T2
	v3 T3
	v4 T4
	v5 T5
}
