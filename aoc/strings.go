package aoc

import (
	"strconv"
	"strings"
)

func ReverseString(s string) string {
	b := []byte(s)
	for i := 0; i < len(b)/2; i++ {
		j := len(b) - i - 1
		b[i], b[j] = b[j], b[i]
	}
	return string(b)
}

// Lines splits a string by newlines.
func Lines(input string) []string {
	return strings.Split(input, "\n")
}

// Itoa is a passthrough for strconv.Itoa
func Itoa(i int) string {
	return strconv.Itoa(i)
}

// Atoi is a passthrough for strconv.Atoi that panics upon failure.
func Atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return i
}

// Atoi is a passthrough for strconv.Atoi that panics upon failure.
func BinAtoi(s string) int {
	i, err := strconv.ParseInt(s, 2, 64)
	if err != nil {
		panic(err)
	}
	return int(i)
}

// Digits parses a string into its consituent digits.
func Digits(s string) []int {
	digits := make([]int, len(s))
	for i, c := range s {
		digits[i] = Atoi(string(c))
	}
	return digits
}

func Characters(s string) []string {
	var res []string
	for _, c := range s {
		res = append(res, string(c))
	}
	return res
}

// IntList parses a list of ints in the format "1 2 3 4 5" or "1,2,3,4,5", or "1, 2, 3, 4, 5".
func IntList(s string) []int {
	var fs []string
	if strings.Contains(s, ", ") {
		fs = strings.Split(s, ", ")
	} else if strings.Contains(s, ",") {
		fs = strings.Split(s, ",")
	} else {
		fs = strings.Fields(s)
	}
	ints := StringToIntList(fs)
	return ints
}

func StringToIntList(s []string) []int {
	ints := make([]int, len(s))
	for i, v := range s {
		ints[i] = Atoi(v)
	}
	return ints
}
