package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test3DRotation(t *testing.T) {
	p := Pos3D{1, 2, 3}
	p2 := p.Rotate(1, 0, 0)
	require.Equal(t, Pos3D{1, 2, 3}, p)
	require.Equal(t, Pos3D{1, -3, 2}, p2)
	p3 := p.Rotate(0, 1, 0)
	require.Equal(t, Pos3D{-3, 2, 1}, p3)
	p = Pos3D{1, 0, 0}
	p = p.Rotate(5, 2, 3)
	require.Equal(t, Pos3D{0, -1, 0}, p)
}

func TestNumpad(t *testing.T) {
	p := Pos3D{1, -2, 3}
	sn := p.Numpad()
	require.Len(t, sn, 9+9+8)
	require.False(t, Contains(sn, p))
	require.True(t, Contains(sn, Pos3D{0, -1, 2}))
}
