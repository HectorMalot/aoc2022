package aoc

type Pos3D struct {
	X, Y, Z int
}

func (p Pos3D) ManhattanDist(q Pos3D) int {
	return Abs(p.X-q.X) + Abs(p.Y-q.Y) + Abs(p.Z-q.Z)
}

func (p Pos3D) Add(x, y, z int) Pos3D {
	return Pos3D{p.X + x, p.Y + y, p.Z + z}
}

func (p Pos3D) Rel(q Pos3D) Pos3D {
	return Pos3D{p.X - q.X, p.Y - q.Y, p.Z - q.Z}
}

func (p Pos3D) Equal(q Pos3D) bool {
	return p.X == q.X && p.Y == q.Y && p.Z == q.Z
}

// Rotates in 90 degree increments, n times per axis, x then y then z
// Rotate around X => X values stay the same
// y becomes z
// z becomes -y
func (p Pos3D) Rotate(x, y, z int) Pos3D {
	for i := 0; i < x; i++ {
		p.Y, p.Z = -p.Z, p.Y
	}
	for i := 0; i < y; i++ {
		p.X, p.Z = -p.Z, p.X
	}
	for i := 0; i < z; i++ {
		p.X, p.Y = p.Y, -p.X
	}
	return p
}

// Returns all neighbours, including diagnonal
func (p Pos3D) Numpad() (list []Pos3D) {
	for dx := -1; dx <= 1; dx++ {
		for dy := -1; dy <= 1; dy++ {
			for dz := -1; dz <= 1; dz++ {
				if !(dx == 0 && dy == 0 && dz == 0) {
					list = append(list, p.Add(dx, dy, dz))
				}
			}
		}
	}
	return list
}

func (p Pos3D) Moves() (list []Pos3D) {
	return []Pos3D{
		p.Add(-1, 0, 0),
		p.Add(1, 0, 0),
		p.Add(0, -1, 0),
		p.Add(0, 1, 0),
		p.Add(0, 0, -1),
		p.Add(0, 0, 1),
	}
}

func BoundingBox3D(ps []Pos3D) (min, max Pos3D) {
	min.X, min.Y, min.Z = int(1e9), int(1e9), int(1e9)
	max.X, max.Y, max.Z = int(-1e9), int(-1e9), int(-1e9)
	for _, p := range ps {
		min.X = Min([]int{min.X, p.X})
		min.Y = Min([]int{min.Y, p.Y})
		min.Z = Min([]int{min.Z, p.Z})
		max.X = Max([]int{max.X, p.X})
		max.Y = Max([]int{max.Y, p.Y})
		max.Z = Max([]int{max.Z, p.Z})
	}
	return
}

func WithinBoundingBox3D(min, max Pos3D) func(Pos3D) bool {
	return func(pd Pos3D) bool {
		return pd.X <= max.X && pd.X >= min.X &&
			pd.Y <= max.Y && pd.Y >= min.Y &&
			pd.Z <= max.Z && pd.Z >= min.Z
	}
}
