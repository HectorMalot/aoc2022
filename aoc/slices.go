package aoc

import (
	"sort"
	"sync"

	"golang.org/x/exp/constraints"
)

// MinimumIndex returns the index of the element that produces the smallest value
// of fn(Element).
func MinimumIndex[T constraints.Integer, E any](elems []E, fn func(E) T) T {
	var i, f, min, mini T
	if len(elems) == 0 {
		return 0
	}

	min = fn(elems[0])
	for i = 1; i < T(len(elems)); i++ {
		if f = fn(elems[i]); f < min {
			min, mini = f, i
		}
	}
	return mini
}

// Sort sorts the provided slice in ascending order
func Sort[E constraints.Ordered](elems []E) []E {
	sort.Slice(elems, func(i, j int) bool { return elems[i] < elems[j] })
	return elems
}

func Map[T1 any, T2 any](list []T1, fn func(T1) T2) []T2 {
	res := make([]T2, 0, len(list))
	for _, e := range list {
		res = append(res, fn(e))
	}
	return res
}

func MapParallel[T1 any, T2 any](list []T1, fn func(T1) T2, threads int) []T2 {
	var wg sync.WaitGroup
	wg.Add(threads)

	// Split the work
	work := [][]T1{}
	step := len(list) / threads
	for i := 0; i < (threads - 1); i++ {
		work = append(work, list[i*step:(i+1)*step])
	}
	work = append(work, list[(threads-1)*step:]) // Last one is special due to int rounding

	// Parallel execution
	res := make([]T2, len(list))
	mutex := sync.Mutex{}
	for i, w := range work {
		from := i * step
		var to int
		if i == threads-1 {
			to = len(list)
		} else {
			to = (i + 1) * step
		}
		go func(in []T1, from, to int) {
			r := Map(in, fn)
			mutex.Lock()
			copy(res[from:to], r)
			mutex.Unlock()
			wg.Done()
		}(w, from, to)
	}
	wg.Wait()
	return res
}

func Inject[T1, T2 any](list []T1, acc T2, fn func(T1, T2) T2) T2 {
	for _, e := range list {
		acc = fn(e, acc)
	}
	return acc
}

func Filter[E any](elems []E, fn func(E) bool) []E {
	var res []E
	for _, e := range elems {
		if fn(e) {
			res = append(res, e)
		}
	}
	return res
}

func Contains[E comparable](elems []E, e E) bool {
	for _, el := range elems {
		if el == e {
			return true
		}
	}
	return false
}

func Reverse[E any](elems []E) []E {
	new := make([]E, len(elems))
	for i, v := range elems {
		new[len(elems)-1-i] = v
	}
	return new
}

// FindFirst returns the index of the element where fn(Element) is true
// Otherwise it returns -1
func FindFirst[E any](elems []E, fn func(E) bool) int {
	if len(elems) == 0 {
		return 0
	}
	for i, v := range elems {
		if fn(v) {
			return i
		}
	}
	return -1
}

func Flatten[E any](slices [][]E) []E {
	l := Inject(slices, 0, func(s []E, l int) int { return l + len(s) })
	res := make([]E, l)
	var head, tail int
	for _, s := range slices {
		tail = tail + len(s)
		copy(res[head:tail], s)
		head = tail
	}
	return res
}

func Pop[E any](slice []E) (E, []E) {
	return slice[len(slice)-1], slice[:len(slice)-1]
}
