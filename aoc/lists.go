package aoc

type LinkedList[T any] struct {
	Head, Tail *Node[T]
	Current    *Node[T]
	length     int
}

type Node[T any] struct {
	Prev  *Node[T]
	Next  *Node[T]
	Value T
}

func NewLinkedList[T any](elems []T) *LinkedList[T] {
	ll := &LinkedList[T]{}
	for _, e := range elems {
		ll.Push(e)
	}
	return ll
}

// Inserts and element at the end of the list
func (ll *LinkedList[T]) Push(v T) {
	if ll.Tail == nil {
		ll.Head = &Node[T]{Value: v}
		ll.Tail = ll.Head
		ll.Current = ll.Head
		ll.length++
	} else {
		ll.Tail.Next = &Node[T]{Value: v, Prev: ll.Tail}
		ll.Tail = ll.Tail.Next
		ll.length++
	}
}

// Inserts an element after the 'current' position
// after the insertion, the current pointer will be on the
// new element
func (ll *LinkedList[T]) Insert(v T) {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}
	if ll.Tail == nil || ll.Current.Next == nil { // empty LL or at last node
		ll.Push(v)
	}
	ll.Current.Next.Prev = &Node[T]{Value: v, Next: ll.Current.Next, Prev: ll.Current}
	ll.Current.Next = ll.Current.Next.Prev
	ll.Current = ll.Current.Next
	ll.length++
}

// Extracts the current element, the 'current' position will be the next position
func (ll *LinkedList[T]) ExtractNode() *Node[T] {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}

	n := ll.Current
	n.Prev.Next = n.Next
	n.Next.Prev = n.Prev
	ll.Current = ll.Current.Next
	ll.length--

	n.Next = nil
	n.Prev = nil
	return n
}

// Extracts the current element, the 'current' position will be the new node
func (ll *LinkedList[T]) InsertNodeBefore(n *Node[T]) {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}
	c := ll.Current
	n.Next = c
	n.Prev = c.Prev
	n.Prev.Next = n
	n.Next.Prev = n
	ll.Current = n
	ll.length++
}

// Inserts an element before the 'current' position
// after the insertion, the current pointer will be on the
// current element
func (ll *LinkedList[T]) InsertBefore(v T) {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}
	if ll.Current.Prev == nil { // at first node
		ll.Current.Prev = &Node[T]{Value: v, Next: ll.Current, Prev: nil}
	} else {
		ll.Current.Prev.Next = &Node[T]{Value: v, Next: ll.Current, Prev: ll.Current.Prev}
	}
	ll.Current.Prev = ll.Current.Prev.Next
	ll.length++
}

func (ll *LinkedList[T]) Len() int {
	return ll.length
}

// Resets the current pointer to the head of the list
func (ll *LinkedList[T]) Reset() {
	ll.Current = ll.Head
}

func (ll *LinkedList[T]) Step(n int) bool {
	for i := 0; i < n; i++ {
		if ll.Current.Next == nil {
			return false
		}
		ll.Current = ll.Current.Next
	}
	return true
}

func (ll *LinkedList[T]) StepBack(n int) bool {
	for i := 0; i < n; i++ {
		if ll.Current.Prev == nil {
			return false
		}
		ll.Current = ll.Current.Prev
	}
	return true
}

func (ll *LinkedList[T]) Inject(fn func(e T, acc T) T) T {
	ll.Current = ll.Head
	acc := ll.Current.Value
	for ll.Step(1) {
		acc = fn(ll.Current.Value, acc)
	}
	return acc
}
