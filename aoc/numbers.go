package aoc

import (
	"golang.org/x/exp/constraints"
)

type Number interface {
	LargeInts | constraints.Float
}

type LargeInts interface {
	~int | ~int32 | ~int64
}

// Abs returns the absolute value of x.
func Abs[T Number](x T) T {
	if x < 0 {
		return -x
	}
	return x
}

// Max returns the largest nummber in the provided slice
func Max[T Number](in []T) T {
	if len(in) == 0 {
		return T(0)
	}
	max := in[0]
	for _, v := range in {
		if v > max {
			max = v
		}
	}
	return max
}

// Min returns the smalles number in the provided slice
func Min[T Number](in []T) T {
	if len(in) == 0 {
		return T(0)
	}
	min := in[0]
	for _, v := range in {
		if v < min {
			min = v
		}
	}
	return min
}

// Sum returns the sum of the provided digits
func Sum[T Number](list []T) T {
	var sum T
	for _, v := range list {
		sum += v
	}
	return sum
}

func WindowFn[T, T2 any](list []T, window int, step int, fn func([]T) T2) []T2 {
	// 1,2,3,4,5
	// 123, 234, 345
	var result []T2
	for i := 0; i <= len(list)-window; i = i + step {
		result = append(result, fn(list[i:i+window]))
	}
	return result
}

// GCD returns the greatest common divisor that can devide the provided numbers(e.g. 8, 12: 4)
func GCD[T LargeInts](a, b T) T {
	a, b = Abs(a), Abs(b)
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

// LCM returns the lowest number divisible by all numbers (e.g. 4,6,8: 24 )
func LCM[T LargeInts](nums ...T) T {
	result := nums[0] * nums[1] / GCD(nums[0], nums[1])
	for _, i := range nums[2:] {
		result = LCM(result, i)
	}
	return result
}

func Factorial[T Number](in T) T {
	if in > 1 {
		return Factorial(in-1) * in
	}
	return 1
}
