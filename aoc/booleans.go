package aoc

// And returns true if all of its arguments are true.
func And(preds ...bool) bool {
	for _, pred := range preds {
		if !pred {
			return false
		}
	}
	return true
}

// Or returns true if any of its arguments are true.
func Or(preds ...bool) bool {
	for _, pred := range preds {
		if pred {
			return true
		}
	}
	return false
}

// Returns v, convenience function for e.g. Map and Inject
func IsTrue(v bool) bool {
	return v
}

// Returns !v, convenience function for e.g. Map and Inject
func IsFalse(v bool) bool {
	return !v
}

func Ternary[T any](e bool, t, f T) T {
	if e {
		return t
	}
	return f
}
