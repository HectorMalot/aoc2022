package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBoardPrintf(t *testing.T) {
	b := NewBoard[bool](10, 10)
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			if (i*j)%3 == 0 {
				b.Set(i, j, true)
			}
		}
	}
	b.Printf(func(v bool) string {
		if v {
			return "#"
		}
		return "."
	})
	require.True(t, b.AllValidCol(0, func(e bool) bool { return e }))
	require.False(t, b.AllValidCol(1, func(e bool) bool { return e }))
}
